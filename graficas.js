d3.json("Practica/practica_airbnb.json").then((featureCollection) => {
  drawMap(featureCollection);
});

function drawMap(featureCollection) {
  console.log(featureCollection);
  console.log(featureCollection.features);
  var width = 800;
  var height = 800;

  var svgmapa = d3
    .select("#map")
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g");

  var center = d3.geoCentroid(featureCollection); //Encontrar la coordenada central del mapa (de la featureCollection)
  //var center_area = d3.geoCentroid(featureCollection.features[0]); //Encontrar la coordenada central de un area. (de un feature)

  var maxDatos = d3.max(
    featureCollection.features,
    (d) => d.properties.avgprice
  );
  var minDatos = d3.min(
    featureCollection.features,
    (d) => d.properties.avgprice
  );
  var contarDatos = d3.count(
    featureCollection.features,
    (d) => d.properties.avgprice
  );

  //to translate geo coordinates[long,lat] into X,Y coordinates.
  var projection = d3
    .geoMercator()
    // .fitSize([width, height], featureCollection) // equivalente a  .fitExtent([[0, 0], [width, height]], featureCollection)
    .scale(98000)
    //Si quiero centrar el mapa en otro centro que no sea el devuelto por fitSize.
    .center(center) //centrar el mapa en una long,lat determinada
    .translate([width / 2, height / 2]); //centrar el mapa en una posicion x,y determinada

  //console.log(projection([long,lat]))

  //Para crear paths a partir de una proyección
  var pathProjection = d3.geoPath().projection(projection);
  //console.log(pathProjection(featureCollection.features[0]))
  var features = featureCollection.features;

  var createdPath = svgmapa
    .selectAll("path")
    .data(features)
    .enter()
    .append("path")
    .attr("d", (d) => pathProjection(d))
    .attr("opacity", function (d, i) {
      d.opacity = 1;
      return d.opacity;
    });

  createdPath.on("click", function (event, d) {
    d.opacity = d.opacity ? 0 : 1;
    d3.select(this).attr("opacity", d.opacity);
    console.log(d.properties.name);
  });
  createdPath.on("click", onClickSelection);
  createdPath.on("mouseover", handleMouseOver).on("mouseout", handleMouseOut);
  // var scaleColor = d3.scaleOrdinal(d3.schemeTableau10);
  // createdPath.attr('fill', (d) => scaleColor(d.properties.name));
  var scaleColor = d3
    .scaleSequential(d3.interpolateBlues)
    .domain([0, contarDatos]);
  createdPath.attr("fill", function (d) {
    if (d.properties.avgprice != undefined) {
      return scaleColor(d.properties.avgprice);
    } else {
      return "#a0a7ae";
    }
  });

  //Creacion de una leyenda

  //Para la leyenda creamos otro lienzo para meter el eje x en la descripcion de la barra

  //La barra de colores las creamos con un gradiante

  //para la escala de colores vamos a crear una funcion que nos de el color segun el precio

  function colorPrecio(precio) {
    return scaleColor(precio);
  }

  function getAvgPrice() {
    preciomedio = [];
    featureCollection.features.forEach((element) => {
      preciomedio.push(element.properties.avgprice);
    });
    return preciomedio;
  }

  // append a defs (for definition) element to your SVG
  var svgLegend = d3.select("#legenda").append("svg").attr("width", 600);
  var defs = svgLegend.append("defs");

  // append a linearGradient element to the defs and give it a unique id
  var linearGradient = defs
    .append("linearGradient")
    .attr("id", "linear-gradient");

  // horizontal gradient
  linearGradient
    .attr("x1", "0%")
    .attr("y1", "0%")
    .attr("x2", "100%")
    .attr("y2", "0%");

  // append multiple color stops by using D3's data/enter step
  linearGradient
    .selectAll("stop")
    .data([
      { offset: "0%", color: colorPrecio(d3.quantile(getAvgPrice(), 0)) },
      { offset: "05%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.2)) },
      { offset: "10%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.4)) },
      { offset: "15%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.4)) },
      { offset: "20%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.7)) },
      { offset: "25%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.75)) },
      { offset: "30%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.8)) },
      { offset: "35%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.9)) },
      { offset: "40%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.92)) },
      { offset: "45%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.95)) },

      { offset: "50%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.97)) },
      { offset: "55%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.98)) },
      { offset: "60%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.98)) },
      { offset: "65%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.98)) },
      { offset: "70%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.98)) },
      { offset: "75%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.98)) },
      { offset: "80%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.98)) },
      { offset: "85%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.99)) },
      { offset: "90%", color: colorPrecio(d3.quantile(getAvgPrice(), 0.99)) },
      { offset: "95%", color: colorPrecio(d3.quantile(getAvgPrice(), 1)) },
      { offset: "100%", color: colorPrecio(d3.quantile(getAvgPrice(), 1)) },
    ])
    .enter()
    .append("stop")
    .attr("offset", function (d) {
      return d.offset;
    })
    .attr("stop-color", function (d) {
      return d.color;
    });

  // append title
  svgLegend
    .append("text")
    .attr("id", "map")
    .attr("x", 10)
    .attr("y", 20)
    .style("text-anchor", "left")
    .text("Precio medio");

  // draw the rectangle and fill with gradient
  svgLegend
    .append("rect")
    .attr("x", 10)
    .attr("y", 30)
    .attr("width", 400)
    .attr("height", 15)
    .style("fill", "url(#linear-gradient)");

  var scaleLinea = d3.scaleLinear().domain([15, 280]).range([0, 400]);
  var linea = d3.axisBottom(scaleLinea);
  linea.ticks(2);
  linea.tickValues([15, 280]);
  //Añadimos eje X
  svgLegend
    .append("g")
    .attr("transform", "translate(10, " + 42 + ")")
    .call(linea);

  var tooltip = d3.select("div").append("div").attr("class", "tooltip");

  function handleMouseOver(event, d) {
    d3.select(this).transition().duration(1000);
    let tooltipText = "";
    if (d.properties.avgprice) {
      tooltipText = `Barrio: ${d.properties.name}, Precio/noche : ${d.properties.avgprice} Eur`;
    } else {
      tooltipText = `Barrio: ${d.properties.name}`;
    }
    tooltip
      .transition()
      .duration(200)
      .style("visibility", "visible")

      .style("left", event.pageX + 20 + "px")
      .style("top", event.pageY - 30 + "px")
      .text(tooltipText);
  }

  function handleMouseOut(event, d) {
    d3.select(this).transition().duration(200).style("stroke-width", 0);

    tooltip.transition().duration(200).style("visibility", "hidden");
  }

  ////Incluimos el otro graficos

  generateBarChar(6);
  function onClickSelection(event, d) {
    d3.select("#barchar1").remove();
    generateBarChar(d.properties.cartodb_id);
  }
  ///ordenamos los datos
  function generateBarChar(id_barrio) {
    var id = id_barrio;

    var data = getNumberBedrooms(id).slice(2, getNumberBedrooms(id).legth);
    var input = [];
    var i = 0;
    data.forEach((element) => {
      input.push({ key: i + " hab.", value: element });
      i++;
    });

    var height = 200;
    var width = 200;
    var marginbottom = 40;
    var margintop = 80;

    var svgbar = d3
      .select("#barchar")
      .append("svg")
      .attr("id", "barchar1")
      .attr("width", width)
      .attr("height", height + marginbottom + margintop)
      .append("g")
      .attr("transform", "translate(0," + margintop + ")");

    //Creacion de escalas
    var xscale = d3
      .scaleBand()
      .domain(
        input.map(function (d) {
          return d.key;
        })
      )
      .range([0, width])
      .padding(0.1);

    var yscale = d3
      .scaleLinear()
      .domain([
        0,
        d3.max(input, function (d) {
          return d.value;
        }),
      ])
      .range([height, 0]);

    //Creación de eje X
    var xaxis = d3.axisBottom(xscale);

    //   //Creacion de los rectangulos
    var rect = svgbar
      .selectAll("rect")
      .data(input)
      .enter()
      .append("rect")
      .attr("fill", "#93CAAE");

    rect
      .attr("x", function (d) {
        return xscale(d.key);
      })
      .attr("y", (d) => {
        return yscale(0);
      })
      .attr("width", xscale.bandwidth())
      .attr("height", function () {
        return height - yscale(0); //Al cargarse los rectangulos tendran una altura de 0
      })
      .on("mouseover", function () {
        // d3.select(this).attr("class", "").attr("fill", "yellow");
        d3.select(this).attr("stroke", "black");
      })
      .on("mouseout", function () {
        // d3.select(this).attr("fill", "#93CAAE");
        d3.select(this).attr("stroke", "none");
      });

    rect
      .transition() //transición de entrada
      //.ease(d3.easeBounce)
      .duration(1000)
      .delay(function (d, i) {
        console.log(i);
        return i * 300;
      })
      .attr("y", (d) => {
        return yscale(d.value);
      })
      .attr("height", function (d) {
        return height - yscale(d.value); //Altura real de cada rectangulo.
      });

    //   //Añadimos el texto correspondiente a cada rectangulo.
    var text = svgbar
      .selectAll("text")
      .data(input)
      .enter()
      .append("text")
      .text((d) => d.value)
      .style("font-size", "12px")
      .attr("x", function (d) {
        return xscale(d.key) + xscale.bandwidth() / 2;
      })
      .attr("y", (d) => {
        return yscale(d.value) - 10;
      })
      .style("opacity", 0);

    //Transicción de entrada en el texto.
    text
      .transition()
      //.ease(d3.easeBounce)
      .duration(500)
      .delay(
        d3.max(input, function (d, i) {
          return i;
        }) *
          300 +
          1000
      )
      .style("opacity", 1);

    //   //Añadimos el eje X
    svgbar
      .append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(xaxis);

    svgbar
      .append("text")
      .attr("x", 95)
      .attr("y", 0 - margintop / 2)
      .attr("text-anchor", "middle")
      .style("font-size", "16px")
      .style("text-decoration", "underline")
      .text(getNumberBedrooms(id).slice(1, 2));

    function getNumberBedrooms(id) {
      arrayHabitaciones = [];

      featureCollection.features.forEach((element) => {
        if (id == element.properties.cartodb_id) {
          arrayAux = [];
          arrayAux.push(element.properties.cartodb_id);
          arrayAux.push(element.properties.name);

          if (element.properties.avgbedrooms[0] != undefined) {
            arrayAux.push(element.properties.avgbedrooms[0].total);
          } else {
            0;
          }

          if (element.properties.avgbedrooms[1] != undefined) {
            arrayAux.push(element.properties.avgbedrooms[1].total);
          } else {
            0;
          }

          if (element.properties.avgbedrooms[2] != undefined) {
            arrayAux.push(element.properties.avgbedrooms[2].total);
          } else {
            0;
          }

          if (element.properties.avgbedrooms[3] != undefined) {
            arrayAux.push(element.properties.avgbedrooms[3].total);
          } else {
            0;
          }

          if (element.properties.avgbedrooms[4] != undefined) {
            arrayAux.push(element.properties.avgbedrooms[4].total);
          } else {
            0;
          }
        }
      });

      return arrayAux;
    }
  }
}
